;; -*- no-byte-compile: t; -*-
;;; ~/.doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:fetcher github :repo "username/repo"))
;; (package! builtin-package :disable t)

;;(package! org2blog :recipe (:fetcher github :repo "org2blog/metaweblog"))
;;(package! xml-rpc-el :recipe (:fetcher github :repo "hexmode/xml-rpc-el"))


;; org2blog package
;; (package! org2blog :recipe (:fetcher github :repo "org2blog/org2blog"))
;;(package! org2blog)
