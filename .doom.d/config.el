;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-


;; Place your private configuration here

;; User Information
(setq user-full-name "Eduardo Robles"
      user-mail-address "eduardorobles02@gmail.com"
      doom-theme 'doom-molokai)

;; Org Mode Configurations
(after! org
  (setq org-directory "~/Nextcloud/Org/"
        org-agenda-files (list "~/Nextcloud/Org/BuJo.org.txt")
        +org-capture-todo-file "~/Nextcloud/Org/BuJo.org.txt"
        +org-capture-notes-file "~/Nextcloud/Notes/notes.org.md"
        org-capture-templates
        '(("t" "Todo" entry (file+headline "~/Nextcloud/Org/BuJo.org.txt" "Tasks")
              "* TODO %?\n  %i\n")
          ("n" "Note" entry (file+headline "~/Nextcloud/Notes/notes.org.md" "Notes")(file "~/.doom.d/templates/notes.orgcaptmpl"))
          ))
  )
